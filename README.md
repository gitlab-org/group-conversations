## GitLab's group conversations

See the latest version at [http://gitlab-org.gitlab.io/group-conversations/](http://gitlab-org.gitlab.io/group-conversations/).

To view locally, run `bin/run`, and then load [http://localhost:8000/](http://localhost:8000/) if it doesn't open automatically.

## Contributing to this project

Please see [CONTRIBUTING.md](CONTRIBUTING.md).
