class: center, middle

# [Plan] group conversation

2019-05-21

Please add questions to the [group conversation Google doc agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU)

.footer[
[Generated with GitLab Pages][project]
]

[plan]: https://about.gitlab.com/direction/plan/
[project]: https://gitlab.com/gitlab-org/group-conversations

---

# [Team updates][team] 👥

- 👋 2019-04-29
  - [Martin Hanzel][martin] joined as Frontend Engineer 🇸🇰
- 👋 2019-05-07
  - [Eugenia Grieff][eugenia] joined as Backend Engineer 🇺🇾🇬🇧
- 😿
  - Victor left; [Eric B][ericb] is interim PM
  - [Pedro] switched to Create
- 🔀
  - [Moved VSM to Manage] (three new BE hires to Manage)
  - [Jan] is [working for Memory] temporarily
  - [Split the backend team] when we hire a new manager

[team]: https://about.gitlab.com/handbook/engineering/development/dev/plan/#plan-team
[Moved VSM to Manage]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22108
[split the backend team]: https://gitlab.com/gitlab-com/Product/issues/178
[working for Memory]: https://gitlab.com/gitlab-com/Product/issues/206

---

# Coming up 🚀

- [View epic tree in epic](https://gitlab.com/gitlab-org/gitlab-ee/issues/10795) - 🙌 [Brett] and [Kushal]
  - First GraphQL feature that uses epics
  - Followed by [drag and drop in the tree](https://gitlab.com/gitlab-org/gitlab-ee/issues/9367)
- [Manually order issues in list view](https://gitlab.com/gitlab-org/gitlab-ee/issues/9121)
  - Ordering in boards is annoying: cards are too tall
  - This ordering carries over to boards
- [Sync into Jira Dev Info API](https://gitlab.com/gitlab-org/gitlab-ee/issues/9643) 🙌 [Heinrich]
  - Replaces [DVCS connector integration](https://docs.gitlab.com/ee/integration/jira_development_panel.html)
  - Only for Jira Cloud right now
- [Board list capacity lines](https://gitlab.com/gitlab-org/gitlab-ee/issues/11403) - 🐶🥫
  - Work towards [board capacity planning](https://gitlab.com/gitlab-org/gitlab-ee/issues/6765)

---

# Backstage 🌠

- [Single codebase](https://gitlab.com/groups/gitlab-org/-/epics/802)
  - ✅ All [backend issues] done
  - 🚧 9/37 [frontend issues] left
- [GraphQL](https://gitlab.com/groups/gitlab-org/-/epics/711)
  - [Monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/48726) added - 🙌 [Jan]
  - [Complexity calculations](https://gitlab.com/gitlab-org/gitlab-ce/issues/58408) improved - 🙌 [Jan]
  - Adding
   [logging](https://gitlab.com/gitlab-org/gitlab-ce/issues/59587) and
   [including Gitaly in
   complexity](https://gitlab.com/gitlab-org/gitlab-ce/issues/58409) -
   🙌 [Charlie]
  - Aim is to [remove the feature flag in 12.1](https://gitlab.com/groups/gitlab-org/-/epics/711#note_171010538)
- [Migrating frontend specs to
  Jest](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Plan&label_name[]=frontend&search=jest) -
  🙌 [Winnie]!

[backend issues]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Plan&label_name[]=backend&label_name[]=single+codebase
[frontend issues]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Plan&label_name[]=frontend&label_name[]=single+codebase

---

# Thinking about 🤔

- [Epics that span top-level groups](https://gitlab.com/gitlab-org/gitlab-ee/issues/11402)
  - Also share labels, milestones, and boards
  - Don't force people to use one top-level group
  - Not purely instance-level so that it works for GitLab.com and
    self-hosted instances
- [Calendar showing due dates](https://gitlab.com/gitlab-org/gitlab-ce/issues/54134)
- [Blocking epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/8764)

---

# Leadership in this space ⭐

- Learn from the industry's [past mistakes](https://about.gitlab.com/direction/plan/#problems-with-existing-project-management-software)
- Provide table stakes missing functionality
  - [Visualize epic dependencies](https://gitlab.com/groups/gitlab-org/-/epics/312)
  - [Epics that span top-level groups](https://gitlab.com/gitlab-org/gitlab-ee/issues/11402)
- Make our product more lovable
  - [More powerful boards](https://gitlab.com/groups/gitlab-org/-/epics/1098)
- Attract other use cases
  - [Quality Management](https://about.gitlab.com/direction/plan/quality_management/)
  - [Requirements Management](https://about.gitlab.com/direction/plan/requirements_management/)
- Increase adoption, starting with [moving multiple issue boards to core](https://gitlab.com/gitlab-org/gitlab-ce/issues/61807)
- [Make it incredibly easy for JIRA users to switch to GitLab](https://gitlab.com/gitlab-org/gitlab-ee/issues/2780) 🧟

---

# Miscellany 🤷

- User autocomplete in comments is faster
  [1](https://gitlab.com/gitlab-org/gitlab-ce/issues/43065)
  [2](https://gitlab.com/gitlab-org/gitlab-ce/issues/60903)
- Search results with Elasticsearch are faster
  [1](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/26342)
  [2](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/9760)
- [Searching in a project is faster](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/26908)
- [Plan social calls](https://gitlab.com/gl-retrospectives/plan/issues/27)
  - Once a week, rotates between two time slots

---

# Recent blog posts ✍

- 🚢 [Fixing task lists] - 🙌 [Brett], [Fatih], and [Victor]
- 🚢  [Elasticsearch lessons learned] - 🙌 [Mario]
- In review: [Migrating to CommonMark] - 🙌 [Brett]
- In review: [Upgrading GitLab to Rails 5] - 🙌 [Jan]
- In review: [Optimizing remote work flexibility] - 🙌 [Jarka]
- In review: [Being a new mom at GitLab] - 🙌 [Jarka]
- WIP: [Developing epics] - 🙌 [Jarka]
- WIP: [Working remotely with children at home] - 🙌 [Sean]

[Fixing task lists]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19114
[Migrating to CommonMark]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19048
[Elasticsearch lessons learned]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19302
[Optimizing remote work flexibility]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15431
[Being a new mom at GitLab]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21410
[Upgrading GitLab to Rails 5]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20527
[Developing epics]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21526
[Working remotely with children at home]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/4440

[felipe]: https://gitlab.com/felipe_artur
[jarka]: https://gitlab.com/jarka
[brett]: https://gitlab.com/digitalmoksha
[jan]: https://gitlab.com/jprovaznik
[mario]: https://gitlab.com/mdelaossa
[heinrich]: https://gitlab.com/engwan
[patrick]: https://gitlab.com/pderichs
[alexandru]: https://gitlab.com/acroitor
[sean]: https://gitlab.com/smcgivern
[pedro]: https://gitlab.com/pedroms
[annabel]: https://gitlab.com/annabeldunstone
[alexis]: https://gitlab.com/uhlexsis
[kushal]: https://gitlab.com/kushalpandya
[constance]: https://gitlab.com/okoghenun
[fatih]: https://gitlab.com/fatihacet
[andré]: https://gitlab.com/andr3
[winnie]: https://gitlab.com/winh
[rajat]: https://gitlab.com/rajatgitlab
[donald]: https://gitlab.com/donaldcook
[ramya]: https://gitlab.com/at.ramya
[walmyr]: https://gitlab.com/wlsf82
[martin]: https://gitlab.com/arthanzel
[ericb]: https://gitlab.com/ebrinkman
[victor]: https://gitlab.com/victorwu
[eugenia]: https://gitlab.com/egrieff
[charlie]: https://gitlab.com/cablett
