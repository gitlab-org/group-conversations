class: center, middle

# [Plan] group conversation

20 November 2018

[Plan]: https://about.gitlab.com/direction/plan/

---

# Team updates 👥

* Backend: [Heinrich Lee Yu][Heinrich] joined on 19 November
    * Based in Cebu, Philippines
* Backend: another candidate currently in reference checks 🤞
* Frontend: [Winnie Hellmann][Winnie] joined from CI/CD!
    * Still in Hannover, Germany!
* Frontend: R. J., starting on 5 December
    * Based in Bangalore, India
* [See the whole team in the handbook][team]

[team]: https://about.gitlab.com/handbook/engineering/dev-backend/plan/#plan-team

---

# [Upgrade to Rails 5] 🚆

* We [switched to Rails 5 in the master branches]
* 11.6 should be [released with Rails 5]
    * We can still revert to Rails 4 before then, or even after
    * But we'd really really like to avoid doing so!
* This was done with great contributions from the community
    * Thanks [blackst0ne] and [jlemaes] in particular!

[Upgrade to Rails 5]: https://gitlab.com/groups/gitlab-org/-/epics/213
[switched to Rails 5 in the master branches]: https://docs.gitlab.com/ce/development/switching_to_rails5.html
[released with Rails 5]: https://gitlab.com/gitlab-org/gitlab-ce/issues/48991

---

# Shipping in 11.5 🚢
## November 22, 2018

* **[Group issue analytics]**
* **[Filter discussions]**
* **[Discussions redesign]**
* **[Issue board card redesign]**
* [Notifications for milestone changes](https://gitlab.com/gitlab-org/gitlab-ce/issues/51520) — 🙌 [Annabel] and [Chantal]!
* [Notifications for epic status changes](https://gitlab.com/gitlab-org/gitlab-ee/issues/7272) — 🙌 [Jarka]!
* [Auto-navigate to last visited issue board](https://gitlab.com/gitlab-org/gitlab-ee/issues/7760) — 🙌 [Brett]!
* [Elasticsearch 6 support](https://gitlab.com/gitlab-org/gitlab-ee/issues/4218) — 🙌 [Mario]!

**[All changes shipping in 11.5](https://gitlab.com/groups/gitlab-org/-/issues?state=closed&label_name%5B%5D=Plan&milestone_title=11.5)**

---

# [Group issue analytics] 📈

.w100[![](group-issue-analytics.png)]

* Thanks [Amelia], [Constance], [Felipe], and [Pedro]! 🙌
* Visualize and filter issues created per month ([example](https://gitlab.com/groups/gitlab-org/-/issues_analytics))
* This is still an early feature; we plan to [iterate] and [improve it](https://gitlab.com/groups/gitlab-org/-/epics/313)

[Group issue analytics]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7478

---

# [Filter discussions] 🔦

.w100[![](filter-discussions.png)]

* Thanks [Constance], [Felipe], and [Pedro]! 🙌
* Available in issues and merge requests. Epics [coming next](https://gitlab.com/gitlab-org/gitlab-ee/issues/7526).

[Filter discussions]: https://gitlab.com/gitlab-org/gitlab-ce/issues/51323

---

# [Discussions redesign] 💬

| Before | After |
|---|---|
| .w100[![](discussions-redesign-before.png)] | .w100[![](discussions-redesign-after.png)] |

* Thanks [Annabel], [Fatih], and [Hazel]! 🙌

[Discussions redesign]: https://gitlab.com/gitlab-org/gitlab-ce/issues/29294

---

# [Issue board card redesign] 🃏

| Before | After |
|---|---|
| .w100[![](issue-board-card-redesign-before.png)] | .w100[![](issue-board-card-redesign-after.png)] |

* Thanks [Constance] and [Pedro]! 🙌
* More redesigns [coming soon](https://gitlab.com/gitlab-org/gitlab-design/issues/83).

[Issue board card redesign]: https://gitlab.com/gitlab-org/gitlab-ce/issues/47008

---

# Slipped in 11.5 😞

* [Promote issue to epic with quick action](https://gitlab.com/gitlab-org/gitlab-ee/issues/3777)
    * We [changed scope after kickoff](https://gitlab.com/gitlab-org/gitlab-ee/issues/3777#note_108162301)
    * We underestimated the scope around handling references and attachments
* [Discussions redesign]
    * While getting this shipped was great, we did have [several regressions](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Plan&label_name[]=regression%3A11.5&label_name[]=frontend)
* [Save issues and merge request lists sort order to the backend](https://gitlab.com/gitlab-org/gitlab-ce/issues/50352)
    * This was scheduled, but it was blocked by [order issues / merge request lists in both directions]
    * It is still blocked by that issue

[order issues / merge request lists in both directions]: https://gitlab.com/gitlab-org/gitlab-ce/issues/39849

---

# Prioritizing security 🔒

Collaborating with Security Team to prioritize security bugs to meet deadlines per [updated process](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15635)

[Issue board with Plan security issues (most are confidential)](https://gitlab.com/groups/gitlab-org/-/boards/796954)

---

# Quality improvements ✅

* [Batch commenting E2E tests](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/7958)
* [Epics E2E tests](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/7381)
* [Epics with milestones API Tests](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/7430)
* [Activity filtering tests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/22564)

---

# Coming up 📅

* [Epic relationships](https://gitlab.com/groups/gitlab-org/-/epics/312)
* [Custom workflows](https://gitlab.com/groups/gitlab-org/-/epics/364)
* [Custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235)

---

class: center, middle

# Conversation time 💬
Speak up or ask questions in chat

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Brett]: https://gitlab.com/digitalmoksha
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa
[Chantal]: https://gitlab.com/crollison
[Heinrich]: https://gitlab.com/engwan

[Pedro]: https://gitlab.com/pedroms
[Annabel]: https://gitlab.com/annabeldunstone
[Kushal]: https://gitlab.com/kushalpandya
[Constance]: https://gitlab.com/okoghenun
[Fatih]: https://gitlab.com/fatihacet
[André]: https://gitlab.com/andr3
[Winnie]: https://gitlab.com/winh
[Ramya]: https://gitlab.com/at.ramya
[Victor]: https://gitlab.com/victorwu

[jlemaes]: https://gitlab.com/jlemaes
[blackst0ne]: https://gitlab.com/blackst0ne

[Amelia]: https://gitlab.com/ameliabauerly
[Hazel]: https://gitlab.com/hazelyang

[iterate]: https://about.gitlab.com/handbook/values/#iteration
