class: center, middle

# Plan update

25 September 2018

[Mario de la Ossa][Mario]

Developer, [Plan]

~~[Sean McGivern](https://gitlab.com/smcgivern)~~

~~Engineering Manager, [Plan]~~

[Plan]: https://about.gitlab.com/handbook/engineering/dev-backend/plan/

---

# Welcome Brett! 👋

We're excited to welcome Brett Walker to our team!

After moving from Germany to Texas, Brett joins us from the Geo team, after their successful migration to Google Cloud.
He brings with him a passion for markdown, specifically CommonMark, which he helped ship in 11.1 and 11.3.

His spare/free time includes developing Versatil Markdown for macOS, and enjoying craft beer and movies.

---

# [Milestone dates in epics]

.w48[![](milestone-dates.png)]

* Thanks [Kushal], [Pedro], and [Mark]!

[Milestone dates in epics]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6470

---

# [Batch comments] (upcoming)

.w48[![](batch-comments.png)]

* Thanks [André] and [Pedro]!

[Batch comments]: https://gitlab.com/gitlab-org/gitlab-ee/issues/1984

---

# 11.3

* [Roadmap and epics list view sorting] - Thanks [Jarka], [Kushal], and [Pedro]!

* [Use ResourceLabelEvent for tracking label changes] - Thanks [Jan], [Fatih], and [Kushal]!

* [Enable CommonMark for files and wikis] - Thanks [Brett]!
    * This allowed us to enable CommonMark across the board!
    
* [Usage ping for different board list types] - Thanks [Chantal]!

* [Use serializer to render diff lines] - Thanks [Felipe]!

* [Fix GitLab branches duplicated in Jira]

[Use ResourceLabelEvent for tracking label changes]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21281
[Enable CommonMark for files and wikis]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21228
[Fix GitLab branches duplicated in Jira]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6752
[Usage ping for different board list types]: https://gitlab.com/gitlab-org/gitlab-ce/issues/48800
[Use serializer to render diff lines]: https://gitlab.com/gitlab-org/gitlab-ce/issues/48084
[Roadmap and epics list view sorting]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6494

---

# 11.4 (upcoming)

* [Filter discussion (tab) by comments or activity in issues and merge requests]
  * Thanks [Felipe], [Pedro], and [Constance]! 

* [System notes generated on epics should link to referenced issues]
  * Thanks [Chantal] and [Annabel]! 

* [Close and reopen epics]
  * Thanks [Jarka], [Annabel], and [Kushal]!
  
* 7 [performance improvements] - Will allow us to reach 100% of our Q3 OKR

[Filter discussion (tab) by comments or activity in issues and merge requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues/26723
[System notes generated on epics should link to referenced issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6318
[Close and reopen epics]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7013
[performance improvements]: https://gitlab.com/groups/gitlab-org/-/epics/247

---

# [Upgrade to Rails 5]

* [We're close]!

* In this cycle we are solving remaining issues for running CI on Rails 4 after switching codebase to Rails 5 
    
* Tentative: [Make Rails 5 default] in master at the beginning of 11.5
    * Fix additional new bugs
    * If Rails 5 is stable, ship 11.5 with it, otherwise revert to 4

* Progress here is thanks to our wonderful community contributors
    * Special thanks to [jlemaes] and [blackst0ne]!

[Upgrade to Rails 5]: https://gitlab.com/groups/gitlab-org/-/epics/213
[We're close]: https://gitlab.com/gitlab-org/gitlab-ce/issues/48991
[gitlab-ce#51729]: https://gitlab.com/gitlab-org/gitlab-ce/issues/51729
[Make Rails 5 default]: https://gitlab.com/gitlab-org/gitlab-ce/issues/48991#note_103294054

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Brett]: https://gitlab.com/digitalmoksha
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa
[Chantal]: https://gitlab.com/crollison

[Pedro]: https://gitlab.com/pedroms
[Annabel]: https://gitlab.com/annabeldunstone
[Kushal]: https://gitlab.com/kushalpandya
[Constance]: https://gitlab.com/okoghenun
[Fatih]: https://gitlab.com/fatihacet
[André]: https://gitlab.com/andr3
[Victor]: https://gitlab.com/victorwu

[Mark]: https://gitlab.com/lulalala

[jlemaes]: https://gitlab.com/jlemaes
[blackst0ne]: https://gitlab.com/blackst0ne
