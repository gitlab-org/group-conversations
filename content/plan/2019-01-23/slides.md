class: center, middle

# [Plan] group conversation

23 January 2019

[plan]: https://about.gitlab.com/direction/plan/

---

# Team updates 👥

- Frontend: [Rajat Jain][rajat] joined on 5 December 🇮🇳
- Backend: Chantal left on 7 December
- Backend: [Patrick Derichs][patrick] joined on 17 January 🇩🇪
- Backend: another candidate in the offer stage 🎉
- Frontend: candidates being reviewed for next hire 🕵️‍️
- [See the whole team in the handbook][team]

[team]: https://about.gitlab.com/handbook/engineering/dev-backend/plan/#plan-team

---

# [Epics of epics] (Ultimate)

.center.w80[![](child-epics.png)]

- Thanks [Annabel], [Jarka], and [Kushal]!
- Customers need multi-level work breakdown structure (WBS)

[Epics of epics]: https://docs.gitlab.com/ee/user/group/epics/

---

# [Roadmap scrolling] (Ultimate)

<video width="100%" autoplay muted loop src="roadmap-scrolling.mov"></video>

- [Recent design collaboration session on YouTube](https://www.youtube.com/watch?v=vdeHsjUtxcc)
- Thanks [Annabel] and [Kushal]!

[Roadmap scrolling]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7325

---

# [Exchange support](Core) 📧

- Microsoft Exchange doesn't support [sub-addressing] (the `+` sign in
  email addresses)
- Almost all of our incoming email handling - Service Desk, issues,
  merge requests - used the `+` and `/` symbols
- We now support two formats:
  - Old: `my-org/great-project@incoming.myorg.com`
  - New: `my-org-great-project-234-issue-@incoming.myorg.com`
- Fully backwards-compatible
- Thanks [Annabel], [Brett], and [Rajat]!

[exchange support]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23523
[sub-addressing]: https://en.wikipedia.org/wiki/Email_address#Sub-addressing

---

# [Import issues from CSV] (Core)

.center.w80[![](import-issues-csv.gif)]

- Thanks [Heinrich], [Pedro], and [Rajat]!
- First iteration of migration strategy for customers who want Jira (or other issue tracker) legacy data in GitLab

[Import issues from CSV]: https://docs.gitlab.com/ee/user/project/issues/#import-issues-from-csv

---

# In progress

## [Start a discussion from a non-discussion comment]

.center.w60[![](start-discussion-non-discussion.gif)]

- Impacts: Epics, Issues and Merge Requests (Discussion tab)
- Shipping in 11.8 🚀
- Thanks [Heinrich] and [Winnie]!

[Start a discussion from a non-discussion comment]: https://gitlab.com/gitlab-org/gitlab-ce/issues/30299

---

# In progress

## [Task list improvements]

.center.w80[![](tasks-lists-improvements.gif)]

- Shipping in 11.8 🚀
- Thanks [Brett] and [Fatih]!

[Task list improvements]: https://gitlab.com/gitlab-org/gitlab-ce/issues/19745

---

# Exciting future

- [Plan direction page](https://about.gitlab.com/direction/plan/)
- [Detailed roadmap of upcoming features](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Aplan)
- See visuals and links to custom fields, portfolio management improvements, value stream management, and other exciting features in the slides at the end of this deck.

---

class: center, middle

# Conversation time 💬

Speak up or ask questions in chat

[felipe]: https://gitlab.com/felipe_artur
[jarka]: https://gitlab.com/jarka
[brett]: https://gitlab.com/digitalmoksha
[jan]: https://gitlab.com/jprovaznik
[mario]: https://gitlab.com/mdelaossa
[heinrich]: https://gitlab.com/engwan
[patrick]: https://gitlab.com/pderichs
[pedro]: https://gitlab.com/pedroms
[annabel]: https://gitlab.com/annabeldunstone
[kushal]: https://gitlab.com/kushalpandya
[constance]: https://gitlab.com/okoghenun
[fatih]: https://gitlab.com/fatihacet
[andré]: https://gitlab.com/andr3
[winnie]: https://gitlab.com/winh
[rajat]: https://gitlab.com/rajatgitlab
[ramya]: https://gitlab.com/at.ramya
[victor]: https://gitlab.com/victorwu

---

# Custom fields

- [Custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235) is a highly demanded feature.
- Plan on implementing [key-value labels](https://gitlab.com/groups/gitlab-org/-/epics/728) first, as the [smallest iteration](https://about.gitlab.com/handbook/product/#the-minimally-viable-change) we can consider.

.center.w60[![](key-value-labels.png)]

---

# Portfolio management

- Launched multi-level child epics in GitLab 11.7 to enable flexible work breakdown structures.
- Improve [usability](https://gitlab.com/groups/gitlab-org/-/epics/134) and [integration of child epics with rest of GitLab](https://gitlab.com/groups/gitlab-org/-/epics/312).
- [Visualize child epics](https://gitlab.com/groups/gitlab-org/-/epics/644) as a tree and in the roadmap.

.center.w60[![](epic-detail-tree.png)]

---

# Next gen issue boards

- [Manage many boards](https://gitlab.com/groups/gitlab-org/-/epics/336).
- [Streamlined rows and columns design](https://gitlab.com/groups/gitlab-org/-/epics/616).
- [Edit issues completely without leaving board](https://gitlab.com/groups/gitlab-org/-/epics/383).

.center.w80[![](in-board-edit.png)]

---

# Enterprise workflows

- [Custom issue workflow states per group](https://gitlab.com/groups/gitlab-org/-/epics/364).
- [Workflow issue board with pre-configured workflow states](https://gitlab.com/groups/gitlab-org/-/epics/424).
- [Value stream analytics in workflow issue board](https://gitlab.com/groups/gitlab-org/-/epics/505).

.center.w70[![](value-stream-board.png)]
