# Edge update

* Date: 22 August 2017
* Who:
  * [Robert Speicher](https://gitlab.com/rspeicher): Senior developer
  * [Christian Couder](https://gitlab.com/chriscool): Senior developer
* Agenda:
  1. Achievements
  1. Git: External ODB support
  1. What's next?
  1. Questions?

---

# Achievements 1/5

* [Project test factories omit repository by default](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13260)
  * Having a repository must now be opted into because it's slower

* [`Project.creator` in tests now defaults to `project.namespace.owner`](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13412)
  * Create less unneeded database records
  * e.g. for `spec/models/project_spec.rb`, 253 fewer DB records created!

* [We were setting up Git repo twice in tests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13123)
  * :facepalm:

* [Reduced our longest-running test file from 45 minutes to 3](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/2719)

---

# Achievements 2/5

* [We now detect and keep track of flaky specs](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13021)
  * Make developers ~~great~~ productive again!

* [Continue migrating Spinach tests to RSpec](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&label_name[]=backstage&label_name[]=technical%20debt&milestone_title=9.5&search=spinach)
  * Thanks [blackst0ne] and [Alexander Randa]!

* We now use the [new cache policies](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/merge_requests/621) in [our `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12978)
  * Faster jobs! Thanks [Nick]!

---

# Achievements 3/5

* [Changelog entries are now categorized](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11579)
  * Thanks [Jacopo Beschi]!

* [New linter to detect forgotten conflict markers](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13141)
  * Thanks [Jen-Shin]!

* [New custom cop to blacklist the use of hash indexes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12869), [New template for database changes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12355), [More database-related documentation](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13466)
  * Thanks [Yorick]!

---

# Achievements 4/5

* [43 test-related merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&milestone_title=9.5&label_name[]=test)

* [83 merged Community Merge Requests in 9.5](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=9.5&sort=created_desc&state=merged)

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/pubchart?oid=16253277&amp;format=interactive"></iframe>

---

# Achievements 5/5

* [Start to put EE-specific files into a top-level `ee/` directory](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/2483)
  * Improve CE/EE code separation and lower the chance of conflicts during the [**daily upstream merge**](https://docs.gitlab.com/ee/development/limit_ee_conflicts.html#daily-ce-upstream-merge)
  * Thanks [Jen-Shin]!

---

# Git: External ODB support 1/2

* A 1-year work by [Christian]
* Roughly equivalent to Git LFS, but baked right into Git itself
  * Based on a Jeff King –aka "Peff"– [2012 branch](https://github.com/peff/git/commits/jk/external-odb-wip)
  * More generic since it allows to define helpers that only need to implement: (`have`,) (`get`,) (`put`, and) `init`
  * Performant thanks to prior work from Lars Schneider's and Ben Peart
  * Let exchange objects in different formats (raw objects or Git objects) or with "direct" access
  * Can be used to get restartable clones

---

# Git: External ODB support 2/2

* Submitted a non-RFC on August 3rd: https://public-inbox.org/git/20170803091926.1755-1-chriscool@tuxfamily.org/
* Documentation: https://public-inbox.org/git/20170803091926.1755-36-chriscool@tuxfamily.org/
* Hopefully will be merged before the end of this year

---

# What's next?

* A few ideas to speed up our test suite:
  * [Consider using `FactoryGirl.build_stubbed` instead of `FactoryGirl.create` since it's way faster](https://gitlab.com/gitlab-org/gitlab-ce/issues/36008)
  * [User association in most factories that belong to a Project could default to the project creator](https://gitlab.com/gitlab-org/gitlab-ce/issues/36009)
  * [Test `NotificationRecipientService` at the correct level](https://gitlab.com/gitlab-org/gitlab-ce/issues/36532)

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/edge

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Nick]: https://gitlab.com/nick.thomas
[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[Yorick]: https://gitlab.com/yorickpeterse
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex
[Jacopo Beschi]: https://gitlab.com/jacopo-beschi
[Build team]: https://about.gitlab.com/handbook/build/
