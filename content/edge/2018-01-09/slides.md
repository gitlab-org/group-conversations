# Edge update

* Date: 09 January 2018
* Who:
  * [Rémy Coutable](https://gitlab.com/rymai): Edge team lead
* Agenda:
  1. Achievements
      1. Community, Performance, Code quality, Documentation, GitLab QA, CE->EE merge, Testing
  1. Concerns
      1. Community
  1. OKRs
  1. Hiring
  1. Questions?

---

# Achievements - Community

* We merged [45 MRs from the community in 10.3](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=10.3&sort=created_desc&state=merged)
* We merged [42 MRs from the community in 10.4](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=10.4&sort=created_desc&state=merged)

<iframe width="800" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqVY7WOWPEqQ-r0Clik5-Dg8iho2REJJkUiF6yDObwm4zx-DjgT2YzyLmT0A-EPU4oCzA1bFndwbiT/pubchart?oid=16253277&amp;format=interactive"></iframe>

– [Check out the spreadsheet](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit?usp=sharing)

---

# Achievements - Community

* [`/unlabel` quick action now limits autocomplete to applied labels](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11110),
  thanks [blackst0ne]!
* [Better empty state for merge requests' `Changes` tab](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15611),
  thanks [blackst0ne]!
* [Add custom brand text on the "New project" page](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15541), thanks [Markus Koller]!
* [Adds ordering to projects contributors in API](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15469), thanks [Jacopo Beschi]!
* [Add support for sorting in Tags API](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15772), thanks [haseeb]!
* [Add pause/resume button to project runners](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16032), thanks [Mario de la Ossa]!
* [Expose participants in MR and Issues API](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16187), thanks [Brent Greeff]!

---

# Achievements - Community

* [James Lopez] is a [new](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9335)
  [Merge Request Coach](https://about.gitlab.com/jobs/merge-request-coach/)! 🎉

---

# Achievements - Performance

* The branches page is 2x faster compared to 10.1:

  <img src="branches-pages-perf-improvements-v2.png" style="width:700px" />

* [Check out the live graph](https://performance.gitlab.net/dashboard/db/gitlab-web-status?panelId=17&fullscreen&orgId=1&from=1510140750035&to=1515423102764&refresh=1m)

* Thanks [Jen-Shin]!

---

# Achievements - Code quality

* Extract the RuboCop configuration from the CE repository into a new
  [`gitlab-styles` gem](https://gitlab.com/gitlab-org/gitlab-styles)
  * [`gitlab-ce`](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14337),
    [`gitlab-qa`](https://gitlab.com/gitlab-org/gitlab-qa/merge_requests/110),
    and [`gitaly`](https://gitlab.com/gitlab-org/gitaly/merge_requests/359)
    already enforce similar code styles!
* [Replace `.team << [user, role]` with `add_role(user)` in specs](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16069), thanks [blackst0ne]!
* [Refactor member view by using presenter](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15715), thanks [TM Lee]!

---

# Achievements - Documentation

* [Add documents for GitLab utilities](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16161), thanks [Robert] and [Jen-Shin]!
* [Add docs about end-to-end testing / GitLab QA tests ](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16043), thanks [Grzegorz]!

---

# Achievements - GitLab QA

* We started to implement a scenario to test deploy keys
  * First iteration is to [register a deploy key](Add a scenario for adding a deploy key)
  * Next step: [Improve the scenario to generate the SSH key pair dynamically
  and actually use it in a CI job](https://gitlab.com/gitlab-org/gitlab-qa/issues/148)

---

# Achievements - CE->EE merge

* Automatization of the CE->EE merge requests works well, we have
[less](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/3946)
[conflicts](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/3949), or even
[zero conflicts](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/3951) sometimes!
  <img src="no-conflict.png" style="width:400px" />

* We now [assign the created MR to the most mentioned person](https://gitlab.com/gitlab-org/release-tools/merge_requests/247),
  and ask people to assign the MR to the next person in line so that it doesn't
  get forgotten. Thanks [Jen-Shin]!
  * Next improvement is to [post a link to the created MR in the `#ce-to-ee` Slack channel](https://gitlab.com/gitlab-org/release-tools/merge_requests/250)

---

# Achievements - Testing

* Running all tests parallelized takes less than 30 minutes on average
  <img src="stable-pipelines-v2.png" style="width:700px" />

  * Thanks [Tomasz] for [improving the performance of our GCE Runners](https://gitlab.com/gitlab-com/infrastructure/issues/3455)!

* We plan to make CE pipelines run under 30 minutes on average for Q1, see OKRs

---

# Concerns - Community

* We started to [analyze why we tend to merge less community contributions](https://gitlab.com/gitlab-org/gitlab-ce/issues/40845):
  * The backlog of Community Contribution is stable at ~190 merge requests
  * The # of Community Contribution we merge declines
  * That means the community is contributing less than in the past

  * [Connor] has suggested [sending a survey to ask directly our community why
    they contribute less than before](https://gitlab.com/gitlab-org/gitlab-ce/issues/40845#note_51174286)

---

# Concerns - Community

* Community MRs / All MRs ratio:

<iframe width="643.0750556269109" height="379.55407969639464" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqVY7WOWPEqQ-r0Clik5-Dg8iho2REJJkUiF6yDObwm4zx-DjgT2YzyLmT0A-EPU4oCzA1bFndwbiT/pubchart?oid=1916723571&amp;format=interactive"></iframe>

* Dropped starting with 9.2, pretty stable since then, that means we merge less MRs in general

---

# OKRs

* [Define the architecture of and produce an end-to-end prototype for a self-service metrics generator](https://gitlab.com/gitlab-org/gitlab-insights/issues/2)
* Complete the work to [make GitLab QA production-ready](https://gitlab.com/gitlab-org/gitlab-qa/issues/126)
* Write 3 GitLab QA tests related to creating and managing Issues
* Write 3 GitLab QA tests related to CI/CD

* [Reduce average CE pipeline duration to 30 minutes](https://gitlab.com/gitlab-org/gitlab-ce/issues/41726)
* [90% of the CE to EE merges have less than 5 conflicts](https://gitlab.com/groups/gitlab-org/-/epics/27)
* Define and schedule high-value issues for [improving the staging test environment](https://gitlab.com/gitlab-com/infrastructure/issues/3177)
* [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
* Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)

---

# Hiring

* Two roles open:
  * [Automation Engineer](https://jobs.lever.co/gitlab/4128d35f-2266-4466-83aa-ca8b64276170)
  * [Test Automation Manager](https://jobs.lever.co/gitlab/0af1d1de-4d2f-4a72-b456-136f477ef9c0)
  * Tell your friends!
* Have begun screening candidates, building the pipeline and ironing out the technical interview process

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/quality/edge/

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Nick]: https://gitlab.com/nick.thomas
[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[Yorick]: https://gitlab.com/yorickpeterse
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex
[Jacopo Beschi]: https://gitlab.com/jacopo-beschi
[Maxim Rydkin]: https://gitlab.com/innerwhisper
[Hiroyuki Sato]: https://gitlab.com/hiroponz
[Build team]: https://about.gitlab.com/handbook/build/
[Axil]: https://gitlab.com/axil
[Markus Koller]: https://gitlab.com/toupeira
[haseeb]: https://gitlab.com/haseebeqx
[Travis Miller]: https://gitlab.com/travismiller
[Richard]: https://gitlab.com/richardc
[Zeger-Jan]: https://gitlab.com/zj
[BJ]: https://gitlab.com/bjgopinath
[Takuya Noguchi]: https://gitlab.com/tnir
[George Andrinopoulos]: https://gitlab.com/geoandri
[Alessio]: https://gitlab.com/nolith
[Ian]: https://gitlab.com/ibaum
[Tomasz]: https://gitlab.com/tmaczukin
[TM Lee]: https://gitlab.com/tmlee
[Mario de la Ossa]: https://gitlab.com/mdelaossa
[Brent Greeff]: https://gitlab.com/brentgreeff
[Connor]: https://gitlab.com/connorshea
[James Lopez]: https://gitlab.com/jameslopez
