# Edge update

* Date: 26 September 2017
* Who:
  * [Rémy Coutable](https://gitlab.com/rymai): Edge team lead
* Agenda:
  1. Community
  1. Achievements
  1. A tale of flaky tests
  1. Issue triage automation
  1. Concerns
  1. Q3 OKRs
  1. Need help
  1. What's on my mind?
  1. Questions?

---

# Community 1/4

* We merged [75 MRs from the community in 10.0](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=10.0&sort=created_desc&state=merged)

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/pubchart?oid=16253277&amp;format=interactive"></iframe>

– [Check out the spreadsheet](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit?usp=sharing)

---

# Community 2/4

* We now have [Fuzzy search](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13780) for issues and merge requests, thanks to [Hiroyuki Sato]!

[![](https://docs.gitlab.com/ee/user/search/img/issue_search_by_term.png)](https://docs.gitlab.com/ee/user/search/img/issue_search_by_term.png)

– [Take a look at the documentation](https://docs.gitlab.com/ee/user/search/index.html#searching-for-specific-terms)

---

# Community 3/4

* We now have ["Filter by my reaction"](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12962) for issues and merge requests, thanks to ... [Hiroyuki Sato]!

<img src="https://gitlab.com/gitlab-org/gitlab-ce/uploads/869f22194724787f029cf1ec2a099b83/filter_by_reaction_13.png" style="width:600px" />

* No wonder why he is [10.0 MVP](https://about.gitlab.com/mvp/)!
---

# Community 4/4

* Decrease [Cyclomatic Complexity threshold to 14](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13972), [Perceived Complexity threshold to 17](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13947), and [ABC threshold to 55.25](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13904)
  * Great effort by [Maxim Rydkin]! Expect [more](https://gitlab.com/gitlab-org/gitlab-ce/issues/31362) [to](https://gitlab.com/gitlab-org/gitlab-ce/issues/31358) [come](https://gitlab.com/gitlab-org/gitlab-ce/issues/28202)!

* [Continue migrating Spinach tests to RSpec](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&label_name[]=backstage&label_name[]=technical%20debt&search=spinach)
  * Thanks [blackst0ne]!

* [17 documentation fixes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20Contribution&milestone_title=10.0&label_name[]=Documentation)!

---

# Achievements

* [Axil] created [a manual job to preview documentation changes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13954)
  * [Check out the documentation](https://docs.gitlab.com/ee/development/writing_documentation.html#previewing-the-changes-live)
  * Pro-tip: If your changes are documentation-only, be sure to
    [name your branch well](https://docs.gitlab.com/ee/development/writing_documentation.html#testing)
    to speed up your pipeline!
      * e.g. `docs/update-api-issues`, `docs-update-api-issues`, `123-update-api-issues-docs`

* [27 test-related merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&milestone_title=10.0&label_name[]=test)

---

# A tale of flaky tests 1/5

## Preliminary informations

* 26K (RSpec) tests, would take **7.5 hours** to run sequentially on the CI
* [Parallelized on 25 jobs](https://docs.gitlab.com/ee/development/testing.html#test-suite-parallelization-on-the-ci),
  this takes between 20 and 30 minutes of wall time

## What's a flaky test?

* It's a test that sometimes fails, but if you retry it enough times, it
  passes, eventually.

---

# A tale of flaky tests 2/5

## Our retry strategy

* When a single test fails, we retry it 3 times:

```
RSpec::Retry: 2nd try ./spec/features/issues_spec.rb:45
```

* After 4 failed attempts, the **test** fails for good
  * This in turn makes the **job** fail
* **But** we're now using the [`retry: 1` option in our `.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html#retry)
* That means a failed **job** is retried once (all the job's tests are then re-run)

---

# A tale of flaky tests 3/5

## Our retry strategy

* Following is a (very) simplified GitLab CE pipeline:

```
- rspec 1 => retried 1 time
  - test1 => retried 3 times
  - test2 => retried 3 times
- rspec 2 => retried 1 time
  - test3 => retried 3 times
  - test4 => retried 3 times
```

* We have a lot less failed pipelines on `master` thanks to these retries!
* But that means, a lot of our tests don't pass on the first try! :(

---

# A tale of flaky tests 4/5

## Keeping track of flaky tests

* On each test run, we have a "current attempt counter" (thanks to
  [`rspec-retry`](https://github.com/NoRedInk/rspec-retry/blob/ed69ee3cfd85c6057dd119e1527423d204eaba32/lib/rspec/retry.rb#L43))
  * Each time a test passes, and the counter is not 1, it means the test
    did pass only after being retried
  * This is the definition of a flaky test, so we keep track of the test

---

# A tale of flaky tests 5/5

## Preventing introduction of flaky tests in `master`

* At the end of a pipeline
  * On `master`, the [`update-tests-metadata` job consolidates the
  newly detected flaky tests](https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/33865801)
  and upload it to S3
  * On other branches the `flaky-examples-check` job warns about the newly
  detected flaky tests
* In the future, when we decide that we have a comprehensive list of tests known
  to be flaky, the `flaky-examples-check` job will fail instead of warn

---

# Issue triage automation

* We now
  [automate part of our triage policies](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=auto%20updated),
  thanks to [Mark]!

  <img src="auto-updated-issues.png" style="width:300px" />

* The auto-triage is currently [done weekly as a scheduled pipeline](https://gitlab.com/gitlab-org/triage/pipelines/12075791/builds)
* There's [an issue to think about how to make this a GitLab feature](https://gitlab.com/gitlab-org/triage/issues/14)
* We plan to [add more policies and features](https://gitlab.com/gitlab-org/triage/issues?label_name%5B%5D=proposal)
  in the future
* The policies [will be published using GitLab Pages](https://gitlab.com/gitlab-org/triage/issues/5)
  so that they'll always reflect what is actually automated
* We [will communicate about this project](https://gitlab.com/gitlab-org/triage/issues/13)
  to get feedback from the community on our policies and how we can improve them

* Learn more and get involved at https://gitlab.com/gitlab-org/triage

---

# Concerns 1/3

* We add about 1,000 new tests per version (total # of tests doubled in 12 months!):

<iframe width="662.5023584905662" height="409.7040676753961" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqVY7WOWPEqQ-r0Clik5-Dg8iho2REJJkUiF6yDObwm4zx-DjgT2YzyLmT0A-EPU4oCzA1bFndwbiT/pubchart?oid=1971270233&amp;format=interactive"></iframe>

– [Check out the spreadsheet](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit?usp=sharing)

---

# Concerns 2/3

* We've spent a lot of time on improving our test suite duration to keep it at
  around 50 minutes (wall time, parallelized), but we should also take a step
  back and improve/remove:
  - Duplicated tests
  - [Heavy-setup tests](https://docs.gitlab.com/ee/development/testing.html#test-speed)
  - [System tests that test all the possible combinations](https://gitlab.com/gitlab-org/gitlab-ce/blob/07f5c3c350f19bcfa4276b28e000cf2a7737ee7e/spec/features/issues/filtered_search/filter_issues_spec.rb)
  - Tests at [the wrong level](https://docs.gitlab.com/ee/development/testing.html#how-to-test-at-the-correct-level)

* Reminder: Testing best practices: https://docs.gitlab.com/ee/development/testing.html#best-practices

---

# Concerns 3/3

* Still [201 outstanding community merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution)
  (including [57 "In Progress"](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&scope=all&search=WIP&sort=created_asc&state=opened))
  * While this stays stable over time, it would be great to reduce those to not
    leave some great contributions behind by lack of attention

* Reminder: It's [every developer's job to review merge requests](https://about.gitlab.com/positions/developer/#responsibilities)

---

# Q3 OKRs

* No progress on [Ship large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149)
* No progress on [Enable Bullet by default on the CI](https://gitlab.com/gitlab-org/gitlab-ce/issues/30129)
* No progress on [GDK based on Kubernetes](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/243)
* No progress on adding more test to GitLab QA (more in next slide)

* Possible reasons:
  * Over-planned given the size of the team
  * The Edge team is frequently asked to help other teams or projects. For example:
      * Mark helped Service support for 2 months
      * Robert helped the Production team for 1 month, and is release manager for 10.0

---

# Need help

* [GitLab QA](https://docs.gitlab.com/ee/development/testing.html#black-box-tests-or-end-to-end-tests) needs a lot more love
  * [Add manual CI job to run tests against staging](https://gitlab.com/gitlab-org/gitlab-qa/issues/10)
  * [Add tests for backup/restore task](https://gitlab.com/gitlab-org/gitlab-qa/issues/22)
  * [Add tests for pushing/pulling images to/from container registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49)
  * [Add tests for Mattermost integration](https://gitlab.com/gitlab-org/gitlab-qa/issues/26)

---

# What's on my mind?

* [High-CPU droplets to get 2x faster pipelines](https://gitlab.com/gitlab-com/infrastructure/issues/2046)
  * Pragmatic move to improve developers productivity
* [Single codebase](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952)
  * That would solve a lot of issues related to the CE->EE upstream merges, but
  could also introduce new issues...

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/edge

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Nick]: https://gitlab.com/nick.thomas
[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[Yorick]: https://gitlab.com/yorickpeterse
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex
[Jacopo Beschi]: https://gitlab.com/jacopo-beschi
[Maxim Rydkin]: https://gitlab.com/innerwhisper
[Hiroyuki Sato]: https://gitlab.com/hiroponz
[Build team]: https://about.gitlab.com/handbook/build/
[Axil]: https://gitlab.com/axil
