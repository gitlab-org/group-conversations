# Discussion update

* Date: 6 March 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=CLzdhM9tMMs)

---

# 8.17 + 9.0 👍

* Shipped [squash](https://gitlab.com/gitlab-org/gitlab-ee/issues/150)
* Shipping
  [reorder issues in issue board](https://gitlab.com/gitlab-org/gitlab-ce/issues/21264)
    * Thanks [Douwe], [Valery], and [Phil]!
* Shipping [v4 of our API](https://gitlab.com/gitlab-org/gitlab-ce/issues/20070)
    * Thanks [Oswaldo], [Toon], [Robert], and others!
    * v3 is deprecated, and will be removed soon

---

# 8.17 + 9.0 👍

* Shipping performance improvements to issues and MR index pages, and notes
  polling: [1](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9006)
  [2](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9030)
  [3](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9036)
    * We haven't deployed 9.0 yet, so I can't show charts 😞
    * Thanks [Felipe], [Jarka], and [Adam]!

---

# 8.17 + 9.0 👎

* It turned out that
  [license finder](https://gitlab.com/gitlab-org/gitlab-ee/issues/1125) (EE)
  needs some CI changes as well, so we switched to working on
  [Service Desk](https://gitlab.com/gitlab-org/gitlab-ee/issues/149) (EE)
* Because of all the breaking changes, we had much less time to work on fixing
  bugs

---

# 9.1 ✨

* [Service Desk](https://gitlab.com/gitlab-org/gitlab-ee/issues/149) (EE)
* [Multiple assignees for issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/13386)
* [Burndown chart](https://gitlab.com/gitlab-org/gitlab-ee/issues/91) (EE)
* [Update issue title in real-time](https://gitlab.com/gitlab-org/gitlab-ce/issues/25051)
    * Once we've verified the
      [ETag support](https://gitlab.com/gitlab-org/gitlab-ce/issues/26926) for
      notes polling performs acceptably

---

# Future goals 🗓

* Fix more bugs!
* Still want to
  [refactor notification settings](https://gitlab.com/gitlab-org/gitlab-ce/issues/24892)
* Deal with nested groups expectation changes
    * [Group level issue boards](https://gitlab.com/gitlab-org/gitlab-ee/issues/928)
      (EE)
* Help Edge team pick up and finish community MRs, as we have a lot that are
  stalled but add interesting features:
  * [Adam] is a merge request coach, as am I
  * [Oswaldo] may help out in future

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Adam]: https://gitlab.com/adamniedzielski
[Jeanine]: https://gitlab.com/jneen
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo

[Toon]: https://gitlab.com/to1ne
[Robert]: https://gitlab.com/razer6
[Douwe]: https://gitlab.com/DouweM
[Phil]: https://gitlab.com/iamphill
