# Discussion update

* Date: 10 May 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead

---

class: two-column

# 9.2 👍

.right[
[![](multiple-assignees.png)](multiple-assignees.png)

[![](slash-commands-preview.png)](slash-commands-preview.png)
]

.left[
* [Multiple assignees](https://gitlab.com/gitlab-org/gitlab-ee/issues/1904)
  (EES)
    * Thanks [Valery]!
* [Comments on personal snippets](https://gitlab.com/gitlab-org/gitlab-ce/issues/15654)
    * Thanks [Jarka]!
* [Display slash commands in preview](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10054)
    * Thanks [Rares] for the initial MR!
    * Thanks [Adam] for finishing it!
]

---

# 9.2 😑

* [Relations between issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/2001)
  (EES) slipped
    * This is OK, we just had too much else to do
* [MR widget refactor](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10319)
  took the entire month (wall time)
    * I know I said this was almost done last time 😬
* We didn't have time to fix many performance issues
* We didn't get to many other issues
    * For example,
      [public API for merge request diff discussions](https://gitlab.com/gitlab-org/gitlab-ce/issues/20901)

---

# 9.3 and beyond 🌌

* [Conversational Development Index](https://gitlab.com/gitlab-org/gitlab-ce/issues/30469)
    * Use usage ping data to give customers benefits, not just us
* [JIRA improvements](https://gitlab.com/groups/gitlab-org/issues?scope=all&&utf8=%E2%9C%93&milestone_title=9.3&label_name%5B%5D=jira)
    * Not in 9.3, but we'd like to be in
      [JIRA's development panel](https://gitlab.com/gitlab-org/gitlab-ee/issues/2299)
    * An [amazing community proxy](https://github.com/dka23/gitlab-github-proxy)
      allows this at the moment
* A lot of things that missed 9.2
    * One example:
      [N+1 query issue on MR indices](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10064)
    * This _just_ missed the freeze
    * Thanks [Felipe]!

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Adam]: https://gitlab.com/adamniedzielski
[Jeanine]: https://gitlab.com/jneen
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo

[Rares]: https://gitlab.com/laleshii
