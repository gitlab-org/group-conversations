# Discussion update

* Date: 19 June 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=8o7cVhDIm68)

---

class: two-column

# 9.3 👍

.right[
[![](convdev.png)](convdev.png)

- - -

[![](snippet-description.png)](snippet-description.png)
]

.left[
* [ConvDev Index](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11377)
    * Thanks [Adam]!
* [Description on personal snippets](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11071)
    * Thanks [Jarka]!
* [Allow checking licenses at the namespace level](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/1961)
    * Let us enable EE features for GitLab.com subscribers only
    * Thanks [Oswaldo]!
* [Enable separate API and web URLs](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11707) for JIRA
    * Thanks [Jarka]!
]

---

class: two-column

# 9.3 👍

.right[
[![](elasticsearch-camel-case.png)](elasticsearch-camel-case.png)

- - -

[![](blame-age.png)](blame-age.png)
]

.left[
* [Elasticsearch improvements for `camelCasedWords`](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/2054)
    * Thanks [Valery]!
* [Colour lines by age in blame view](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/7198)
    * From the community
    * Thanks [Jeff]!
    * Thanks [Adam] and [Annabel] for reviewing!
]

---

# 9.3 😟

* [Relations between issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/2001)
  (EES) slipped again
    * Much, much more information available:
        * [Retrospective for related issues](https://docs.google.com/document/d/1ecTfNanxL6Sd7ePM6piWZGqn5ADWHNmLuPv0kNTx93Q)
        * [9.3 retrospective](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit#heading=h.iz21dkd5wroi)
* We need to make
  [merging MRs more reliable](https://gitlab.com/gitlab-org/gitlab-ce/issues/31207)
    * Unhandled failures currently lock the MR for 24 hours, which is very
      annoying
* Still haven't done
  [public API for merge request diff discussions](https://gitlab.com/gitlab-org/gitlab-ce/issues/20901)
    * This is really important for API consumers

---

# 9.4 and beyond 🌌

* [Native group milestones](https://gitlab.com/gitlab-org/gitlab-ce/issues/30126)
    * Q: Don't we already have group milestones?
    * A: No, we fake it by grouping milestones with the same title
    * Enables [group issue boards](https://gitlab.com/gitlab-org/gitlab-ee/issues/928)
* [Declarative policies](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10515)
    * A new DSL for developers to use when setting permissions
    * Make our policy framework easier to extend for EE
    * Speed up permission checking for groups of users, without having to
      duplicate logic
* [Slack app for GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/32167)
    * Slack apps can't be configured to allow this for other GitLab installs
    * We're hopeful that will be possible soon, though!

---

# 9.4 and beyond 🌌

* [JIRA development panel](https://gitlab.com/gitlab-org/gitlab-ee/issues/2299)
  integration
    * An [amazing community proxy](https://github.com/dka23/gitlab-github-proxy)
      allows this at the moment
    * Current plan: do the same thing by providing an endpoint that pretends to
      be GitHub Enterprise
* [Create a commits table](https://gitlab.com/gitlab-org/gitlab-ce/issues/30224)
    * For MRs with a lot commits, and a lot of pipelines, we can spend over 60%
      of the load time just deserialising the Yaml!
    * Making this a real table will remove a lot of that time 📉
* Related issues will be there (it's in master)

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Adam]: https://gitlab.com/adamniedzielski
[Jeanine]: https://gitlab.com/jneen
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo

[Annabel]: https://gitlab.com/annabeldunstone
[Jeff]: https://gitlab.com/brunsa2
