# Discussion update

* Date: 30 April 2018
* Who: [Jan Provaznik][Jan] ~~[Sean McGivern](https://gitlab.com/smcgivern)~~
  * [Discussion backend](https://about.gitlab.com/handbook/backend/#discussion)
  senior developer ~~engineering manager~~

---

# Hiring 👍👍⏲

* Target for Q2: hire 3 developers
  * [Chantal] started on 23 April 2018!
  * MC is starting on 7 May 2018!
  * One more to go 🚀
* Target for Q2: source 150 candidates
  * Not started yet

---

# Elasticsearch 🔍

* [Developer documentation] added!
* [Code search for kebab-case identifiers] fixed!
  * Before: `kebab-case` matched `kebab` and `case` as separate words
  * Now: treated as a single identifier: `kebab-case`
  * Also: improved other similar cases (as in the MR description)
* Thanks [Mario]! And [Valery] and [Nick] for great reviews!

---

# Subgroups support 🏷

* [Group boards now show subgroup issues]
* This also means you can use labels from parent groups in subgroups
* Labels flow 'down' to subgroups
* Issues flow 'up' to parent group boards and views
* Thanks [Felipe]!

---

# Elsewhere 🗺

* Merge request performance improvements
  * [Fix Gitaly N+1 when viewing an MR]
  * [Reduced Redis usage from diff caching]
  * [Another Gitaly N+1 identified on main MR page]
* [Comment thread in epics]
  * Thanks [Jan] and [Kushal]!
* [Custom additional text for emails]
  * Thanks [Mario] and [Eric]!
* [Add closed by information to issue API]
  * Thanks [Oswaldo] and [Haseeb]!

---

# Coming up in 10.8 📅

* [Email notifications] and [user autocompletion] in epics
* [Improved notification settings]
* [Burndown chart for group milestones]
* [API around merge request and commit disussions]

---

# Coming up in 11.0 📅

* [Team dashboards]
* [Batch commenting in merge requests] (still)
* Make more progress on [Rails 5] support


[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa
[Chantal]: https://gitlab.com/crollison

[Valery]: https://gitlab.com/vsizov
[Nick]: https://gitlab.com/nick.thomas
[Kushal]: https://gitlab.com/kushalpandya
[Eric]: https://gitlab.com/MadLittleMods
[Haseeb]: https://gitlab.com/haseebeqx

[Developer documentation]: https://docs.gitlab.com/ee/development/elasticsearch.html
[Code search for kebab-case identifiers]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/5138

[Group boards now show subgroup issues]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18187

[Fix Gitaly N+1 when viewing an MR]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18383
[Another Gitaly N+1 identified on main MR page]: https://gitlab.com/gitlab-org/gitlab-ce/issues/45190
[Reduced Redis usage from diff caching]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/17746
[Comment thread in epics]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/4997
[Custom additional text for emails]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/5031
[Add closed by information to issue API]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/17042

[Team dashboards]: https://gitlab.com/gitlab-org/gitlab-ee/issues/4830
[Batch commenting in merge requests]: https://gitlab.com/groups/gitlab-org/-/epics/23
[Rails 5]: https://gitlab.com/gitlab-org/gitlab-ce/issues/14286
[Email notifications]: https://gitlab.com/gitlab-org/gitlab-ee/issues/5480
[user autocompletion]: https://gitlab.com/gitlab-org/gitlab-ee/issues/4084
[API around merge request and commit disussions]: https://gitlab.com/gitlab-org/gitlab-ce/issues/20901
[Improved notification settings]: https://gitlab.com/gitlab-org/gitlab-ce/issues/44230
[Burndown chart for group milestones]: https://gitlab.com/gitlab-org/gitlab-ee/issues/3064
