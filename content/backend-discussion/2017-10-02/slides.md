# Discussion update

* Date: 2 October 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=9YsTEIHmBwU)

---

# 10.0 🛳

* [Group issue boards](https://gitlab.com/gitlab-org/gitlab-ee/issues/928) shipped!
  * I'm using this, it's great!
  * Thanks [Felipe] and [Simon]!
* [JIRA development panel integration for commits](https://gitlab.com/gitlab-org/gitlab-ee/issues/2381)
  * Thanks [Oswaldo]!
* [First-time contributor badge](https://gitlab.com/gitlab-org/gitlab-ce/issues/35161)
  * Thanks [Micael]!

---

# 10.0 🛳

* [Filter issues / MR by your emoji reaction](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12962)
  * Thanks [Hiroyuki]!
* [Allow resolving outdated comments on push](https://gitlab.com/gitlab-org/gitlab-ce/issues/36994)
  * Thanks [Ashley]!
* [`/move` quick action](https://gitlab.com/gitlab-org/gitlab-ce/issues/26925)
  * Thanks [Manolis]!
* [Include time tracking information in CSV export](https://gitlab.com/gitlab-org/gitlab-ee/issues/1406)
  * Thanks [Bohdan]!

---

# Conference season 📛

* [Jarka] and [Douglas] (among others!) at
  [EuRuKo](https://www.euruko2017.org/) 🇭🇺
* [Felipe] and [Oswaldo] (among others!) at
  [The Conf](http://www.theconf.club/) 🇧🇷

---

# Concerns

* [Lock issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/18608)
  * Missed 10.0, will be in 10.1
* [Rails 5 upgrade](https://gitlab.com/gitlab-org/gitlab-ce/issues/14286)
  * Two people isn't enough
  * The longer we leave it, the more we have to do
  * We will try to get
    [more people dedicated to this in 10.3](https://gitlab.com/gitlab-org/gitlab-ce/issues/14286#note_41641110)

---

# [Comment on images](https://gitlab.com/gitlab-org/gitlab-ce/issues/35873) 😮

.right[
Thanks [Felipe]!

Thanks [Clement]!

Thanks [Bryce]!
]

.w60[![](image-diff-commenting.png)]

---

# [Saved board config](https://gitlab.com/gitlab-org/gitlab-ee/issues/2518) 💾

.right[
.w80[![](saved-board-configuration.png)]
]

.left[
Thanks [Oswaldo]!

Thanks [Simon]!
]

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron

[Simon]: https://gitlab.com/psimyn
[Clement]: https://gitlab.com/ClemMakesApps
[Bryce]: https://gitlab.com/brycepj

[Hiroyuki]: https://gitlab.com/hiroponz
[Ashley]: https://gitlab.com/AshleyDumaine
[Manolis]: https://gitlab.com/frite
[Bohdan]: https://gitlab.com/g3dinua
