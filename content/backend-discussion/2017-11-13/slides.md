# Discussion update

* Date: 13 November 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=SYrNBu4b81A)

---

# 10.1 + 10.2 🛳

* [Comment on image diffs](https://gitlab.com/gitlab-org/gitlab-ce/issues/35873)
    * Thanks [Felipe] and [Clement]!
* [Lock issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/18608)
    * Thanks [Jarka] and [Luke]!
* [Saved board config](https://gitlab.com/gitlab-org/gitlab-ee/issues/2518)
    * Thanks [Oswaldo] and [Simon]!

---

# [Epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3254)

* EEU feature
* Exist at the group level, not the project level
* Can group a set of issues together
* Part of portfolio management (including roadmaps, etc.)

---

# Team changes ↔

* [Valery] and [Douglas] are now full-time on the Geo team!
* We have one person joining at the start of December
* ... and [we're hiring](https://jobs.lever.co/gitlab/5899df3c-4690-4c3a-a77d-1fe281a26d8a)
  for another!
* Immediate consequences:
    * We can't devote enough people to the
      [Rails 5 upgrade](https://gitlab.com/gitlab-org/gitlab-ce/issues/14286) in
      10.3, so we'll revisit in future
    * Same with a lot of
      [Elasticsearch improvements](https://gitlab.com/gitlab-org/gitlab-ee/issues/1607)
      needed to work towards GitLab.com readiness

---

# 10.3+ 🚴

* [Comment on commits in a merge request](https://gitlab.com/gitlab-org/gitlab-ce/issues/31847)
    * Currently, comments on commits in an MR go away after a rebase
    * This will add the ability to associate a comment with both the commit and
      the MR it's in
* More
  [epics and portfolio management](https://gitlab.com/gitlab-org/gitlab-ee/issues/3254)
* Add specs in GitLab QA:
    * [Protected branches](https://gitlab.com/gitlab-org/gitlab-qa/issues/77)
    * [Squash and rebase](https://gitlab.com/gitlab-org/gitlab-qa/issues/78)
* [Finalise merge request schema changes](https://gitlab.com/gitlab-org/gitlab-ce/issues/39533)

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron

[Simon]: https://gitlab.com/psimyn
[Clement]: https://gitlab.com/ClemMakesApps
[Luke]: https://gitlab.com/lbennett

[Hiroyuki]: https://gitlab.com/hiroponz
[Ashley]: https://gitlab.com/AshleyDumaine
[Manolis]: https://gitlab.com/frite
[Bohdan]: https://gitlab.com/g3dinua
