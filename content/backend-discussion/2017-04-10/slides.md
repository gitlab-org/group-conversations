# Discussion update

* Date: 10 April 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=nSWLHIbwioc)

---

class: two-column

# 9.1 🚤

.right[
[![](issues-dashboard.png)](issues-dashboard.png)

[![](merge-requests-dashboard.png)](merge-requests-dashboard.png)

[![](etag-caching.png)](etag-caching.png)
]

.left[
* Issue and MR index pages got a lot faster:
  [1](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9006)
  [2](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9030)
  [3](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9036)
    * Thanks [Jarka] and [Felipe]!
* Issue notes polling now
  [skips the DB ~70% of the time](https://gitlab.com/gitlab-org/gitlab-ce/issues/29777)
    * Still hits Redis and Unicorn
    * Thanks [Adam]!
* The GitHub importer is
[more robust](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10207),
[much faster](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10273), and
can be
[run through Rake instead of Sidekiq](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10308)
    * Helps customers and large OSS projects move to GitLab
    * Thanks [Douglas], [James L], and [Gabriel]!
]

---

class: two-column

# 9.1 👍

.right[
[![](burndown-chart.png)](burndown-chart.png)

[![](service-desk.png)](service-desk.png)
]

.left[
* Shipped
  [burndown charts](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/1540) (EES)
    * Thanks [Felipe]!
* Shipped
  [Service Desk](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/1508) (EEP)
    * Can only be enabled by an admin
    * Email an issue to a project, without an account!
    * Emails become confidential issues, and replies are sent to the emailer
    * Uses the existing permissions, spam checking, etc. code
    * Hard to show this visually 🙂
    * Thanks [Jeanine] and [Felipe]!
]

---

# 9.1 😑

* [Multiple assignees for issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/13386)
  didn't quite make it
    * This is fine - it's a big change
    * No time to go through review cycles and merge by the 7th
* [Backporting usage ping to CE](https://gitlab.com/gitlab-org/gitlab-ce/issues/23361)
  isn't finished
    * Cross-team effort
    * Will be in 9.1.0
* Leftover performance issues:
    * [Projects::IssuesController#show](https://gitlab.com/gitlab-org/gitlab-ce/issues/27164)
    * [Projects::MergeRequestsController#show](https://gitlab.com/gitlab-org/gitlab-ce/issues/27166)
    * [Projects::MilestonesController#show](https://gitlab.com/gitlab-org/gitlab-ce/issues/27387)
        * SQL trace is 1.7 MB 🙀
* [MR widget refactor](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10319)
  isn't quite there yet
    * But it will be awesome when it's done! 💪

---

# 9.2 ✨

* We'll finish
  [multiple assignees for issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/13386)
  (EE)
* Starting work on
  [relationships between issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/2001)
  (EE)
* Make our public API able to do everything the UI can for
  [merge request diff discussions](https://gitlab.com/gitlab-org/gitlab-ce/issues/20901)
* Finishing
  [allow comments on personal snippets](https://gitlab.com/gitlab-org/gitlab-ce/issues/15654)
    * Fun fact: [Jarka] started work on this before she joined GitLab
    * It's going to be great!

---

# Future goals 🗓

* [Refactor notification settings](https://gitlab.com/gitlab-org/gitlab-ce/issues/24892)
    * Unblocks adding new notification types that are opt-in (like pipeline
      emails would have been, had we had this in place at the time)
* [Create a table joining commit SHAs to MRs](https://gitlab.com/gitlab-org/gitlab-ce/issues/30224)
    * Can't remove a commit to reclaim storage if it was referenced in an MR
    * Can't remove a commit to redact sensitive information either

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Adam]: https://gitlab.com/adamniedzielski
[Jeanine]: https://gitlab.com/jneen
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo

[James L]: https://gitlab.com/jameslopez
[Gabriel]: https://gitlab.com/brodock
