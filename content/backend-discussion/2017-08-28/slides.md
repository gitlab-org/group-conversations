# Discussion update

* Date: 28 August 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead
* [View on Youtube](https://www.youtube.com/watch?v=UnEghMF9zKc)

---

# 9.5

* Low capacity, didn't ship any big features
* [JIRA settings](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12050)
  are now simpler
    * Thanks [Jarka]!
* [Merge requests should no longer get stuck as locked](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13207)
    * We can improve this further still, but we'll clean up after two hours now
      if the background job no longer exists
    * Thanks [Oswaldo]!
* We are migrating merge request diffs and commits in the background from the
  old format to the new format

---

# Group issue boards

.w80[
  ![](group-issue-boards.png)
]

* Missed 9.5, will be in 10.0
* Thanks [Felipe] and [Simon]!

---

# 10.0 🚀

* [Show commits in JIRA development panel](https://gitlab.com/gitlab-org/gitlab-ee/issues/2381)
* [Lock issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/18608)
* [Speed up merge requests with lots of resolved discussions](https://gitlab.com/gitlab-org/gitlab-ce/issues/35475)
* [Show rebase errors to users](https://gitlab.com/gitlab-org/gitlab-ee/issues/1774)
* [Filter issues / MR by your emoji reaction](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12962)
    * Thanks [Hiroyuki]!

---

# Personnel

* [Jeanine] is no longer with GitLab
* [Micael] has joined!
  * He's working on a
    [first-time contributor badge](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13143)
    for someone's first MR to a project
  * Then: other things!
* Hiring one more backend developer for Discussion, and one for Platform (both
  backfills)

---

# What's next 🌌

* [Comment on image diffs](https://gitlab.com/gitlab-org/gitlab-ce/issues/35873)
* [Show the MR a commit was added in](https://gitlab.com/gitlab-org/gitlab-ce/issues/2383)
    * Thanks again [Hiroyuki] for picking this up!
* We still have a lot of areas to improve performance
    * For instance,
      [updating a merge request from a push](https://gitlab.com/gitlab-org/gitlab-ce/issues/35914)
      is slow

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron
[Jeanine]: https://gitlab.com/jneen

[Simon]: https://gitlab.com/psimyn
[Hiroyuki]: https://gitlab.com/hiroponz
