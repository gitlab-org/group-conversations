# Discussion update

* Date: 24 July 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead

---

# Related issues

.w80[
  ![](related-issues.gif)
]

* It took a while, but we shipped it
* Thanks [Oswaldo]!

---

# Slack app for GitLab.com

.w80[
  ![](slack-app.png)
]

* Thanks [Valery]!
* Still working on getting this in the Slack directory

---

# 🚤 🚤 🚤

.w80[
  ![](issues-show.png)
]

* Displaying an issue got a lot faster
* We can go faster still!
* Thanks [Jeanine]!

---

# Group milestones

* Not fully feature complete yet, but enables
  [group issue boards](https://gitlab.com/gitlab-org/gitlab-ee/issues/928)
* Thanks [Felipe]!

---

# Backstage

* Gullwing
  * Internal project for our sales and marketing teams
  * [Jarka] is crushing it
* [Declarative policies](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10515)
  * A new DSL for developers to use when setting permissions
  * The cause of that issues improvement
  * Thanks [Jeanine]!

---

# Slow merge requests 😴

.w80[
  ![](merge-request-slow-methods.png)
]

* Checking if we had fetched the HEAD reference from git
  * Now [handled in the DB](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12424), thanks [Adam]!
* Highlighting diff discussions -
  [most of these are collapsed](https://gitlab.com/gitlab-org/gitlab-ce/issues/35475)!

---

# Slow merge requests 😴

![](merge-request-all-commit-shas.png)

* Deserialising every commit from YAML to find
  [pipelines for the MR](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546/pipelines)
* `column_shas` is the new approach; `serialised_shas` is the old
* Only applies to new pushes - 9.5 will migrate existing MR versions

---

# Edge team 💖

* Performance bar has directly enabled faster iteration on performance issues
  * That was the line profiler on the last slide
  * Thanks [Rémy]!
* We mangle some Japanese text quite badly when guessing diff encoding
  * Thanks [Jen-Shin] for being all over this!
* 💕 for [Christian], [Mark], and [Robert] too!

---

# Personnel

* [Adam] is no longer with GitLab
* We're [hiring again](https://about.gitlab.com/jobs/developer/)

---

# What's next 🌌

* [Group issue boards](https://gitlab.com/gitlab-org/gitlab-ee/issues/928)
* More Gullwing!
* [Stop MRs getting locked for 24 hours](https://gitlab.com/gitlab-org/gitlab-ce/issues/31207)
  * Reduce the lock time
  * Be able to check the actual job status: enqueued, started, failed, etc.
* [Show rebase errors to users](https://gitlab.com/gitlab-org/gitlab-ee/issues/1774)
* [Make creating MRs and issues faster](https://gitlab.com/gitlab-org/gitlab-ce/issues/32844)

[Valery]: https://gitlab.com/vsizov
[Douglas]: https://gitlab.com/dbalexandre
[Felipe]: https://gitlab.com/felipe_artur
[Adam]: https://gitlab.com/adamniedzielski
[Jeanine]: https://gitlab.com/jneen
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo

[Rémy]: https://gitlab.com/rymai
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool
[Mark]: https://gitlab.com/markglenfletcher
[Robert]: https://gitlab.com/rspeicher
