# Discussion update

* Date: 17 January 2018
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  engineering manager
* [View on Youtube](https://www.youtube.com/watch?v=nT7fX4O6Urs)

---

# [Reorder issues in epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3694)

.w90[![](reorder-issues-in-epics.gif)]

* Thanks [Jarka] and [Clement]!

---

# [Dashboard 🚤](https://gitlab.com/gitlab-org/gitlab-ce/issues/37143)

* This was going to show a speed improvement on the dashboard
* Sadly, that didn't happen (yet)
    * We made one query much faster, and removed a useless one
    * Then we found another (almost) useless one
    * We are close to making this work!
* Thanks [Felipe]!

---

# [Search 🚤](https://gitlab.com/gitlab-org/gitlab-ce/issues/40540)

* Likewise, this was going to show a speed improvement to search
* Sadly, that didn't happen (yet)
    * The query itself is improved, but only for certain inputs
    * For other searches, the result set is simply too large
    * We are close to making this work!
* Thanks [Jan]!

---

# [Uploads to object storage](https://gitlab.com/gitlab-org/gitlab-ee/issues/4163)

* Challenging for a few reasons:
    * [Hofstadter's Law](https://en.wikipedia.org/wiki/Hofstadter%27s_law): It
      always takes longer than you expect, even when you take into account
      Hofstadter's Law
    * A
      [schema description](https://gitlab.com/gitlab-org/gitlab-ee/blob/8fcbabeef8fb5144150119b771aba184ad917a63/doc/development/file_storage.md#path-segments)
      which this slide is too small to contain
    * Pop quiz: what's the different between `tmp/upload` and `tmp/uploads`?
    * In general, there was technical debt and complexity we didn't prepare for
      properly when scoping out the issue
* Thanks [Micael] for working hard on this!

---

# API improvements

* [Group issue boards API](https://docs.gitlab.com/ee/api/group_boards.html)
* [Epics API](https://docs.gitlab.com/ee/api/epics.html)
    * [Epic issues API](https://docs.gitlab.com/ee/api/epic_issues.html)
* Thanks [Felipe] and [Jarka]!

---

# [Q1 OKRs](https://about.gitlab.com/okrs/)

* Discussion: Make sure all
  [Discussion backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Discussion)
  are merged, closed, labeled “awaiting feedback”, or taken over by us and in
  active development
* Discussion: Resolve all
  [Security SL1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SL1),
  [Support SP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SP1),
  and
  [Availability AP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=AP1)
  issues
* Discussion: Close 36
  [Discussion backend bug](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Discussion&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened)
  issues. Afterwards, we should verify that the backlog went _down_ from the 280
  we started with, because otherwise bugs are getting reported faster than we
  can fix them, and we are not making a dent.
* Discussion: Ship 100% of committed
  [deliverable issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=Deliverable)
  each release
* Discussion: Ship Rails 5 migration

---

# Hiring

* Plan is to double the team size by hiring six developers throughout the year
* [Tell your friends!](https://jobs.lever.co/gitlab/5899df3c-4690-4c3a-a77d-1fe281a26d8a)

---

# Coming up 📆

* More [portfolio management](https://gitlab.com/gitlab-org/gitlab-ee/issues/3254)
    * [Comment thread on epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3889)
    * [View subgroup epics in epic list](https://gitlab.com/gitlab-org/gitlab-ee/issues/4301)
    * [Roadmaps](https://gitlab.com/gitlab-org/gitlab-ee/issues/3559)
* [Batch commenting in MRs](https://gitlab.com/gitlab-org/gitlab-ee/issues/1984)
    * We will start this later in 10.5
    * Aim to ship in 10.6
* Figure out a way to make
  [diff collapsing better](https://gitlab.com/gitlab-org/gitlab-ce/issues/36051)

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron
[Jan]: https://gitlab.com/jprovaznik

[Clement]: https://gitlab.com/ClemMakesApps
